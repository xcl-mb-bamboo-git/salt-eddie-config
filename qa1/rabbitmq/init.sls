# These are the supported pillars with their defaults. This can be overridden in the customer repository.

rabbitmq:
  version: "3.5.4-1"
  secret_key:
  plugin:
    rabbitmq_management:
      - enabled
    rabbitmq_management_visualiser:
      - enabled
  policy:
    rabbitmq_policy:
      - name: HA
      - pattern: '.*'
      - definition: '{"ha-mode": "all"}'
  vhost:
    virtual_host:
      - owner: admin
      - conf: .*
      - write: .*
      - read: .*
  user:
    admin:
      - password: password1
      - force: True
      - tags: admin, administrator
      - perms:
        - '/':
          - '.*'
          - '.*'
          - '.*'
      - runas: root
  cluster:
    name: eddies
    enabled: False
    user: admin
    host: 192.168.100.12
  rabbitmq_ac_config:
    guest_enabled: True
{#- vim:ft=sls
-#}
