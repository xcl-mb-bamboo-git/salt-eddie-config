# Overwrite default release info.

tomcat_config:
  # name has to be specified however remaining values have defaults
  name: 'apache-tomcat-8.0.22'
