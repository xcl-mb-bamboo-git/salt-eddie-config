include:
  - dev.java
  - dev.tomcat
  - dev.walletservice
  - dev.edge
  - dev.engine
  - dev.dataservice
  - dev.rabbitmq
  - dev.federator
  - dev.documentation
  - dev.admin
  - dev.eventmgmt
  - dev.monitor
