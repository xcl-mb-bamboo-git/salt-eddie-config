customer:
  'G@customer_code:EDDIE and G@environment:dev':
    - match: compound
    - dev.release

  'G@customer_code:EDDIE and G@environment:qa1':
    - match: compound
    - qa1.release


  'G@customer_code:EDDIE and G@environment:stg':
    - match: compound
    - stg.release
