  ds_connection:
    type: "CASSANDRA_ENGINE"
    jdbc_driver: "com.mysql.jdbc.Driver"
    jdbc_url: "jdbc:mysql://mdb01s1stg.cix.xcl.ie:3306/test_db?autoReconnectForPools=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&zeroDateTimeBehavior=convertToNull&serverTimezone=UTC&tcpKeepAlive=true&tcpNoDelay=true&dumpQueriesOnException=true&includeInnodbStatusInDeadlockExceptions=true"
    user_name: "dbunit"
    password: "dbunit"

  cassandra_wallet:
    type: "CASSANDRA_WALLET"
    cassandra_keyspace: "matchbook"
    cassandra_connection_points: "casdb01S1stg.cix.xcl.ie"
    cassandra_properties: "{'class': 'SimpleStrategy', 'replication_factor': 2} AND DURABLE_WRITES = true"


  cassandra_engine:
    type: "CASSANDRA_ENGINE"
    cassandra_keyspace: "matchbook"
    cassandra_connection_points: "casdb01S1stg.cix.xcl.ie"
    cassandra_properties: "{'class': 'SimpleStrategy', 'replication_factor': 2} AND DURABLE_WRITES = true"

  event_bus:
    type: "EVENT_BUS"
    event_bus_uri: "amqp://rabbitmq01S1stg.cix.xcl.ie"
    queues:
      component_engine: "ENGINE"
      queues_engine: "mb.engine.queue"
      threads: "6"
      durable: "false"
      bindings: "mb.engine.#"
      prefetch_count: "100"
      queues_engine_2: ""
      threads_2: "1"
      bindings_2: "mb.federator"
      prefetch_count_2: "100"

  data_service:
    component: "DATA_SERVICE"
    queues:
      queues_ds: "mb.dataservice.queue"
      threads: "6"
      durable: "false"
      bindings: '"mb.dataservice.#", "mb.broadcast.#"'
      prefetch_count: "100"

  federator:
    component: "FEDERATOR"
    queues:
      queues_fed: "mb.federator.queue"
      threads: "1"
      durable: "false"
      bindings: '"mb.broadcast.created.market","mb.federator"'
      prefetch_count: "100"

  bet_bot:
    component: "BETBOT"
    queues:
      queues_betbot: "mb.betbot.queue"
      threads: "1"
      durable: "false"
      bindings: '"mb.broadcast.snapshot.created", "mb.broadcast.model.cleaned","mb.broadcast.wallet.account.create","mb.broadcast.book.created"'
      prefetch_count: "100"

  admin:
    component: "ADMIN"
    queues:
      queues_admin: "mb.admin.queue"
      threads: "6"
      bindings: '"mb.broadcast.#"'
      prefetch_count: "100"

  edge:
    component: "EDGE"
    queues:
      queues_edge: '""'
      threads: '1'
      bindings: '"mb.broadcast.#","mb.dataservice.book.create"'
      prefetch_count: "100"

  event_management:
    component: "EVENT_MANAGEMENT"
    queues:
      queues_event_management: "mb.eventmanagement.queue"
      threads: "6"
      bindings: '"mb.eventmgmt.#"'
      prefetch_count: "100"

  wallet_server:
    component: "WALLET_SERVER"
    queues:
      queues_wallet_server: "mb.wallet.queue"
      threads: "1"
      bindings: '"mb.wallet.#","mb.broadcast.account.added","mb.broadcast.dataservice.login"'
      prefetch_count: "100"

  settings:
    exchange_name: "matchbook"
    exchange_durable: "true"
    offers_prefetch: "1000"
    offers_queues_per_engine: "8"
