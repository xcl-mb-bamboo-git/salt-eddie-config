## Release info ###
###################
include:
  - stg.java
  - stg.tomcat
  - stg.walletservice
  - stg.dataservice
  - stg.edge
  - stg.engine
  - stg.rabbitmq
  - stg.federator
  - stg.documentation
  - stg.admin
  - stg.monitor

### Nexus Certified platform artifact ###
certified_platform_base_url: 'http://hippo.xcl.ie:2345/nexus/content/sites/certified_platform'
certified_platform_version: '1.0'

### base installation directory ###
base_install_dir: /xanadu

### checksums ###
install_packages_dir: /xanadu/install_packages

### log location ###
base_log_dir: /xanadu/logs

### Atlassian hipchat details ###
hipchat:
  enabled: true
  auth_token: XiJCzwWjRIdsrt5jfzQ3lHDG5hgYXDvL7WwvkWt1
  room_id: 1824723

### Components ###
component_base_url: http://hippo.xcl.ie:2345/nexus

stg:
  walletservice:
    component: walletservice
  edge:
    component: edge
  engine:
    component: engine
  dataservice:
    component: dataservice
  federator:
    component: federator
  documentation:
    component: documentation
  admin:
    component: admin
  eventmgmt:
    component: eventmgmt
  monitor:
    component: monitor
